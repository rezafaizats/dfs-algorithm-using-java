import kotlin.reflect.jvm.internal.impl.utils.DFS;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class DFSExample {

    static ArrayList<Node> nodes = new ArrayList<>();
    static class Node {
        int data;
        boolean visited;
        List<Node> neighbours;

        Node (int data){
            this.data = data;
            this.neighbours = new ArrayList<>();
        }

        public void addNeighbours(Node neighbourNode){
            this.neighbours.add(neighbourNode);
        }

        public List<Node> getNeighbours(){
            return neighbours;
        }

        public void setNeighbours(List<Node> neighbours){
            this.neighbours = neighbours;
        }
    }

    //Adjacency Matrix
    public ArrayList<Node> findNeighbours(int adjacency_matrix[][], Node x){
        int nodeIndex =- 1;

        ArrayList<Node> neighbour = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++){
            if (nodes.get(i).equals(x)){
                nodeIndex = 1;
                break;
            }
        }

        if (nodeIndex != -1){
            for (int j = 0; j < adjacency_matrix[nodeIndex].length; j++){
                if (adjacency_matrix[nodeIndex][j] == 1){
                    neighbour.add(nodes.get(j));
                }
            }
        }
        return neighbour;
    }

    //Recursive DFS Matrix

    public void dfs(int adjacency_matrix[][], Node node){
        System.out.print(node.data + " ");
        ArrayList<Node> neighbours = findNeighbours(adjacency_matrix, node);
            node.visited = true;

        for (int i = 0; i < neighbours.size(); i++){
            Node  n = neighbours.get(i);

            if (n != null && !n.visited){
                dfs(adjacency_matrix, n);
            }
        }
    }

    //Iterative DFS Matrix

    public void dfsUsingStack(int adjacency_matrix[][], Node node){
        Stack<Node> stack = new Stack<>();
        stack.add(node);

        while (!stack.isEmpty())
        {
            Node element = stack.pop();
            if(!element.visited)
            {
                System.out.print(element.data + " ");
                element.visited = true;
            }

            ArrayList<Node> neighbours=findNeighbours(adjacency_matrix,element);
            for (int i = 0; i < neighbours.size(); i++) {
                Node n=neighbours.get(i);
                if(n!=null &&!n.visited)
                {
                    stack.add(n);
                }
            }
        }
    }

    //Recursive DFS
    public void dfs(Node node){
        System.out.print(node.data + " ");
        List<Node> neighbour = node.getNeighbours();
            node.visited = true;

        for (int i = 0; i < neighbour.size(); i++){
            Node n = neighbour.get(i);

            if (n != null && !n.visited){
                dfs(n);
            }
        }
    }

    //Iterative DFS using stack
    public void dfsUsingStack(Node node){
        Stack<Node> stack = new Stack<Node>();
        stack.add(node);

        while (!stack.isEmpty()){
            Node element = stack.pop();

            if (!element.visited){
                System.out.print(element.data + " ");
                element.visited = true;
            }

            List<Node> neighbour = element.getNeighbours();
            for (int i = 0; i < neighbour.size(); i++){
                Node n = neighbour.get(i);

                if (n != null && !n.visited){
                    stack.add(n);
                }
            }
        }
    }

    public static void main(String arg[]){
        Node node40 = new Node(40);
        Node node10 = new Node(10);
        Node node20 = new Node(20);
        Node node30 = new Node(30);
        Node node60 = new Node(60);
        Node node50 = new Node(50);
        Node node70 = new Node(70);

        //Activate this for stack technique
//        node40.addNeighbours(node10);
//        node40.addNeighbours(node20);
//        node10.addNeighbours(node30);
//        node20.addNeighbours(node10);
//        node20.addNeighbours(node30);
//        node20.addNeighbours(node60);
//        node20.addNeighbours(node50);
//        node30.addNeighbours(node60);
//        node60.addNeighbours(node70);
//        node50.addNeighbours(node70);

        //Activate this for matrix technique
        nodes.add(node40);
        nodes.add(node10);
        nodes.add(node20);
        nodes.add(node30);
        nodes.add(node60);
        nodes.add(node50);
        nodes.add(node70);
        int adjacency_matrix[][] = {
                {0,1,1,0,0,0,0},  // Node 1: 40
                {0,0,0,1,0,0,0},  // Node 2 :10
                {0,1,0,1,1,1,0},  // Node 3: 20
                {0,0,0,0,1,0,0},  // Node 4: 30
                {0,0,0,0,0,0,1},  // Node 5: 60
                {0,0,0,0,0,0,1},  // Node 6: 50
                {0,0,0,0,0,0,0},  // Node 7: 70
        };

        DFSExample dfsExample = new DFSExample();

        System.out.println("The DFS traversal of the graph using stack : ");
        dfsExample.dfsUsingStack(adjacency_matrix, node40);

        System.out.println();

        //Reset node value
        node40.visited = false;
        node10.visited = false;
        node20.visited = false;
        node30.visited = false;
        node50.visited = false;
        node60.visited = false;
        node70.visited = false;

        System.out.println("The DFS traversal of he graph using recursion");
        dfsExample.dfs(adjacency_matrix, node40);
    }

}
